/**
 * UTILITY 
 *   register with Discord the commands in utils.js
 *   $npm run register
 * DISCORD-SIDE PERMISSIONS
 *   SCOPES
 *     applications.commands, bot
 *   PERMISSIONS
 *     Send Messages, Manage Roles
 *   BOT-USER PERMISSION
 *     restricted to specific channels (role registration channel)
 * LINKS
 *   library discord-interactions-js        https://github.com/discord/discord-interactions-js
 *   discord developer portal               https://discord.com/developers/applications
 *   discord developer portal documentation https://discord.com/developers/docs/intro
 */


import 'dotenv/config';
import express from 'express';
import {InteractionType, InteractionResponseType, MessageComponentTypes} from 'discord-interactions';
import {VerifyDiscordRequest, DiscordRequest} from './utils.js';


// ------------------------------------------
// Server Setup 
// ------------------------------------------
const app = express();
const PORT = process.env.PORT || 3000;
app.use(express.json({ verify: VerifyDiscordRequest(process.env.PUBLIC_KEY) }));


// ------------------------------------------
// Utility Functions
// ------------------------------------------
async function trysend(user_id, guild_id, selected_role_id) {
  const globalEndpoint = `/guilds/${guild_id}/members/${user_id}/roles/${selected_role_id}`
  try {
    const res = await DiscordRequest(globalEndpoint, {
      method: 'PUT',
      //body: {intents: 268435456},
    });
  } catch (err) {
    console.error('Error trying to send: ', globalEndpoint);
    console.error('Error trying to send: ', err);
  }
} 


// ------------------------------------------
// Endpoints
// ------------------------------------------
app.post('/interactions', function (req, res) {
  const { type, data } = req.body;

  if (type === InteractionType.APPLICATION_COMMAND) {
    if (data.name === 'role') {
      return res.send({
        type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE,
        data: {
          content: 'Select user roles.',
          components: [
            {
              components: [
                {
                  custom_id: 'role-selection-menu',
                  max_values: 1,
                  min_values: 1,
                  options: [
                    {
                      label: 'Phasmophobia',
                      description: 'Enroll in Phasmophobia shenannigans',
                      value: '1107820379411583007',
                      emoji:{
                        id: '1124696601420632074',
                        name: 'Phasmophobia'
                      },
                    },
                    {
                      label: 'Jackbox Party',
                      description: 'Enroll in Jackbox shenannigans',
                      value: '1108542675868717116',
                      emoji:{
                        id: '1129562368465182760',
                        name: 'Jackbox Party'
                      },
                    },
                    {
                      label: 'Dungeons and Dragons',
                      description: 'Enroll in Dungeons and Dragons shenannigans',
                      value: '1108543095152324719',
                      emoji:{
                        id: '625891304148303894',
                        name: 'Dungeons and Dragons'
                      },
                    },
                    {
                      label: 'Final Fantasy XI',
                      description: 'Enroll in Final Fantasy XI shenannigans',
                      value: '1108543355228524616',
                      emoji:{
                        id: '1111653026466373733',
                        name: 'Final Fantasy XI'
                      },
                    },
                    {
                      label: 'Magic The Gathering',
                      description: 'Enroll in Magic The Gathering shenannigans',
                      value: '1129883771286589521',
                      emoji:{
                        id: '1129794983843606599',
                        name: 'Final Fantasy XI'
                      },
                    },
                  ],
                  placeholder: 'Pick your interests.',
                  type: 3
                }
              ],
              type: 1
            }
          ],
        }
      });

    }
  }

  if (type === InteractionType.MESSAGE_COMPONENT) {
    if (data.custom_id === 'role-selection-menu') {
      const user_id = req.body.member.user.id;
      const guild_id = req.body.channel.guild_id;
      const selected_role_id = data.values[0];

      trysend(user_id, guild_id, selected_role_id);

      return res.send({
        type: InteractionResponseType.CHANNEL_MESSAGE_WITH_SOURCE
      });
    }
  }
});


// ------------------------------------------
// Start
// ------------------------------------------
app.listen(PORT, () => {
  console.log('Listening on port 3000');
});
