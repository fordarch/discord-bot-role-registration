import 'dotenv/config';
import { capitalize, InstallGlobalCommands } from './utils.js';

// Provide prompt for user role registration
const ROLE_REGISTRATION = {
  name: 'role',
  description: 'Prompt message for users to register their role.',
  type: 1,
};

const ALL_COMMANDS = [ROLE_REGISTRATION];

InstallGlobalCommands(process.env.APP_ID, ALL_COMMANDS);